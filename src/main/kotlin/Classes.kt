fun main() {
    val child = Person("Child", "Child", 1)
    val p1 = Person("Lolo", "last", 22, child)
    println(p1.firstName)
    println(child.lastName)
    val rectangle = Rectangle(5.0, 3.0)
    println("The perimeter is ${rectangle.perimeter}")
    val rectangle2 = Rectangle(5.0, 3.0)

    println(rectangle==rectangle2)
}


//Primary constructor первичный конструктор
class Person(val firstName: String, val lastName: String, var age: Int) {
    var children: MutableList<Person> = mutableListOf() //ArrayList

    init {
        println("Person is created $firstName")
    }

    constructor(firstName: String, lastName: String, age: Int, child: Person) : this(firstName, lastName, age) {
        children.add(child)
    }

    // конструктор без аргументов
    constructor() : this("", "", -1)
}

data class Rectangle(var height: Double, var lenght: Double) {
    var perimeter = (height + lenght) * 2

    var test = 1
        get() = field + 1
        set(value) {
            if (value < 0) println("Negative value")
            field = value
        }

    fun area() = height + lenght
}
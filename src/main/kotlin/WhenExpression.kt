
fun main(){
    testWhen(1)
    testWhen("hello")
    testWhen(18)
}

fun testWhen(input: Any){
    when(input){
        1-> println("One")
        2-> println("Two")
        in 10..20 -> println("щт 10 до 20")
        is String -> println("Была введена строка ${input.length}")
        else -> println("some")
    }
}
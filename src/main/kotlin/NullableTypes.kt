fun main() {
    var a: String = "Hello"
    // a = null ///NPE

    println(a.length)

    var b: String? = "Test"
    b = null
    // b.length

    //вернет длинну строки, если нулл то вернет нулл
    b?.length

    /// Elvis operator ?:

    val l = b?.length ?: -1

    b = if ((0..10).random()>5) "laksdfalk" else null
    //выбрасывает NPE если переменная налл
    val t = b!!.length


}